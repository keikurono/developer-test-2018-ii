# primeros 2 bytes :
# 00 = login      , 
# 01 = posicion
# 02 = evento
#
########### caso login:
# respuesta: 100
# 
########### caso posicion:
# respuesta: si es valido=111, si es invalido=110
# dato | bytes
# id = 2-9
# timestamp = 10-19
# sur o norte = 20 (SUR vuelve negativo la coordenada)
# lat = 21-27
# oeste o este = 28 (OESTE vuelve negativo la coordenada)
# lon = 29-35
# validez = 36 (A=valido,V=invalido)
# velocidad = 37-38
# 
#
#
########### caso evento
#respuesta : 120
# dato | bytes
# output_code = 2-3
# output_value = 4-5
#~ require 'active_support/all'
require 'date'
class Decoder

	@id
	@datetime
	@speed
	@valid
	@lat
	@lon
	@response
	@pack

	attr_accessor :id, :datetime, :speed, :valid, :lat, :lon, :response, :output_code, :output_value

	def initialize

	end

	def read(pack)
		@pack = pack[0..1]
		if @pack == "01"
			@id = pack[2..9]
			
			@datetime = Time.at(pack[10..19].to_i).strftime("%Y-%m-%d %H:%M:%S")
			
			@lat = pack[21..27].to_f
			@lon = pack[29..35].to_f
			
			if pack[20] == "S"
				@lat = -1*@lat
			end
			if pack[28] == "W"
				@lon = -1*@lon
			end
			
			@valid = pack[36]
			@speed = pack[37..38]
			
			if @valid == "A"
				@response = "111"
			else
				@response = "110"
			end
			
		elsif @pack == "02"
			@output_code = pack[2..3]
			@output_value = pack[4..5]		
			@response = "120"
		end
		self
	end

	def login
		if @pack == "00"
			@response = "100"
			return true
		end  
		return false
	end
end
